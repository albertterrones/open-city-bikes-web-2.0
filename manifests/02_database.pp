class { '::mysql::server':
    root_password  => 'opencitybikes',
    override_options => {
        mysqld => {
            'bind_address' => '0.0.0.0',
        }
    }
}
mysql::db { 'opencitybikes':
    user     => 'opencitybikes',
    password => 'opencitybikes',
    host     => '%',
    charset  => 'utf8',
}

class {'::mongodb::server':
    port    => 27017,
}

mongodb::db { 'opencitybikes':
    user => 'opencitybikes',
    password => 'opencitybikes',
}
