OpenCityBikes
========================

En aquest README s'explica la instal·lació i configuració del projecte web de OpenCityBikes en un entorn LAMP.


Requeriments del projecte
-------------------------

Per instal·lar el sistema web del OpenCityBikes abans cal preparar un entorn Linux basat en Debian amb els següents serveis:

  * Servidor web Apache o Nginx amb virtualhost cap a la carpeta /web del source del O

  * Motor PHP carregat al servidor web

  * Client Git

  ````
sudo apt-get install php5-dev php5-cli php-pear

  ````

Instal·lació i configuració del projecte web
--------------------------------------------

  * Descàrrega del projecte Git
  ````  
git clone https://<username>@bitbucket.org/albertterrones/open-city-bikes-web-2.0.git

  ````

  * Descàrrega del gestor de paquets Composer
  ````
curl -s https://getcomposer.org/installer | php
  ````

  * Instal·lació dels Vendors
  ````
php composer.phar install
  ````

  * Configuració de les dades de connexió de la BDD
Editant el fitxer /app/config/parameters.yml del source de OpenCityBikes

  * Configuració dels permisos de app/cache i app/logs
  ````
./init.sh
  ````

  * Creació de la BDD
  ````
php app/console doctrine:database:create
  ````

  * Mapeig de les Entitats a la BDD i refresc
  ````
php app/console doctrine:schema:update --force
  ````

Altres recursos útils en el desenvolupament amb Symfony2.7
----------------------------------------------------------

  * Creació d'un Bundle amb nom "bikes" a la organització "OPENCITY"
  ````
php app/console generate:bundle --namespace=OPENCITY/bikesBundle --format=yml
  ````

  * Creació d'un Entity amb un asistent per terminal
  ````
php app/console doctrine:generate:entity
  ````

  * Scaffolding (CRUD)
  ````
php app/console generate:doctrine:crud --entity=OPENCITYbikesBundle:<EntityName> --with-write --no-interaction --overwrite [--format=yml/annotation]
  ````

  * Mapeig de les Entitats a la BDD i refresc
  ````
php app/console doctrine:schema:update --force
  ````

Entorn de demo
--------------
http://openbike.byte.cat
